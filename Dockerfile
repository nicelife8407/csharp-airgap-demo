# https://hub.docker.com/_/microsoft-dotnet
# FROM gitlab.cdneks.com:5050/root/dotnetapp/sdk:5.0 AS build
# WORKDIR /source

# # copy csproj and restore as distinct layers
# COPY *.csproj .
# RUN dotnet restore

# # copy and publish app and libraries
# COPY . .
# RUN dotnet publish -c release -o /app --no-restore

# final stage/image
FROM gitlab.cdneks.com:5050/root/dotnetapp/runtime:5.0
WORKDIR app
COPY app .
ENTRYPOINT ["dotnet", "dotnetapp.dll"]
